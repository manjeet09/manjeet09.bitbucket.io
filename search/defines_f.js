var searchData=
[
  ['qbit_5ftcp_5fhdr_5foffset_0',['QBIT_TCP_HDR_OFFSET',['../ekd__tcp__api_8h.html#aa64624a7178d17a7f0787e06d9f23241',1,'ekd_tcp_api.h']]],
  ['qbit_5ftcp_5fshort_5fhdr_5fsize_1',['QBIT_TCP_SHORT_HDR_SIZE',['../ekd__tcp__api_8h.html#a465c981523cba4083252e2dbaa4c53a2',1,'ekd_tcp_api.h']]],
  ['qbit_5ftcp_5fshort_5fmax_5fpkt_5flen_2',['QBIT_TCP_SHORT_MAX_PKT_LEN',['../ekd__tcp__api_8h.html#ad06b97bb971c57cf2d98d7a50583085a',1,'ekd_tcp_api.h']]],
  ['qbit_5ftcp_5fshort_5foffset_3',['QBIT_TCP_SHORT_OFFSET',['../ekd__tcp__api_8h.html#a67818374aa1cdf17ccc1d99679898939',1,'ekd_tcp_api.h']]],
  ['qbit_5ftcp_5fshort_5fpayloadmaxlen_4',['QBIT_TCP_SHORT_PAYLOADMAXLEN',['../ekd__tcp__api_8h.html#a094d7e0bf35ba890a74c07e7a1c30cf2',1,'ekd_tcp_api.h']]],
  ['qdma_5fmax_5fports_5',['QDMA_MAX_PORTS',['../ekd__api_8h.html#aa44ad336f11b6edaf5634fdc30af14a8',1,'QDMA_MAX_PORTS():&#160;ekd_api.h'],['../ekd__util_8h.html#aa44ad336f11b6edaf5634fdc30af14a8',1,'QDMA_MAX_PORTS():&#160;ekd_util.h']]],
  ['queue_5fbase_5foffset_6',['QUEUE_BASE_OFFSET',['../ekd__api_8h.html#a918e890061fac3b79b99725434561a5b',1,'ekd_api.h']]]
];
