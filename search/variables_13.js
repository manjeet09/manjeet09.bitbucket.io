var searchData=
[
  ['s_5fw_5fkey_5findex_5fcontrol_0',['s_w_key_index_control',['../structsystem__register.html#ac03ba540b79cf9511efeec1d647b9a9e',1,'system_register']]],
  ['s_5fw_5ftrigger_5faction_5fcontrol_5fregister_1',['s_w_trigger_action_control_register',['../structsystem__register.html#a51d0377215572292989a64782dd49e32',1,'system_register']]],
  ['sack_5fperm_2',['sack_perm',['../structtcp__option__sackperm__h.html#ada19e8e20d25735e3bcd77cd00e5768b',1,'tcp_option_sackperm_h::sack_perm()'],['../structtcp__params.html#a8654f8db09454509c12969969e6499da',1,'tcp_params::sack_perm()']]],
  ['scale_3',['scale',['../structtcp__option__ws__h.html#a6ba9f5ccbadc9f87fb6f1a1e58688663',1,'tcp_option_ws_h']]],
  ['seq_4',['seq',['../structtcp__option__sack__h.html#ab0fbeeb851937b85138de5b64a9d4b6f',1,'tcp_option_sack_h']]],
  ['seqnum_5',['seqnum',['../structrx__priority__queue.html#a75e53ecb7aa6a0cf9c7185b1ac9749c9',1,'rx_priority_queue::seqnum()'],['../structxmit__pio__queue__data.html#abd38d499a4758ab4d6ea203b5c02e79b',1,'xmit_pio_queue_data::seqnum()'],['../structxmit__queue__data.html#a961b2acebd95c477a12bf2cc38bd301c',1,'xmit_queue_data::seqnum()'],['../structxmit__queue__data__mbuf.html#a31ee9991f17e764d824989f5cb783687',1,'xmit_queue_data_mbuf::seqnum()'],['../structtcp__params.html#a1dd545a7dedf4f246e9a0e75d90b4e99',1,'tcp_params::seqnum()']]],
  ['seqnum_5fbk_6',['seqnum_bk',['../structtcp__params.html#a5280b13a7542a6667f67e278bdd219da',1,'tcp_params']]],
  ['seqnum_5frollover_5fcount_7',['seqnum_rollover_count',['../structtcp__params.html#ad2652b52b1176e1fee4fb45c0eb8bcd8',1,'tcp_params']]],
  ['short_5fkey_5fpriority_8',['short_key_priority',['../structtcp__tx__dma__header.html#a748db828373367d73b096282a2af2411',1,'tcp_tx_dma_header']]],
  ['sleep_5ftimer_5fkni_9',['sleep_timer_kni',['../structkni__json__config.html#a2efed3da4095b542bd654566b3ffcb4d',1,'kni_json_config']]],
  ['software_5fcount_10',['software_count',['../ekd__util_8h.html#ae6a411307de3448f8712450ba52b322b',1,'ekd_util.h']]],
  ['source_11',['source',['../structudphdr.html#a57e57d722b5f11510e4ae3608877f316',1,'udphdr::source()'],['../structtcphdr.html#ac3e6c13e4fefb7a5fa91d27b9aac2d3c',1,'tcphdr::source()']]],
  ['sourceip_12',['sourceIp',['../structTCP__row__data__buf.html#aa61af2374aaeb2b2fce7c2e9efe37b54',1,'TCP_row_data_buf']]],
  ['sourceport_13',['sourcePort',['../structTCP__row__data__buf.html#a9119762eb26501576720dd33c49cd69f',1,'TCP_row_data_buf']]],
  ['src_5fip_14',['src_ip',['../structtcp__tx__data__connection__idx__lookup.html#aa2ac60f814889504833457464ab1fbc2',1,'tcp_tx_data_connection_idx_lookup::src_ip()'],['../structtcp__rx__data__filter__lookup.html#abb6165248e2d632a534f0559f3d4d7e1',1,'tcp_rx_data_filter_lookup::src_ip()']]],
  ['src_5fip_5fmatch_5fdis_15',['src_ip_match_dis',['../structtcp__rx__data__filter__lookup.html#aa4c0a66b47b7496ab664d4a8b1012a7c',1,'tcp_rx_data_filter_lookup']]],
  ['src_5fmac_16',['src_mac',['../structtcp__tx__data__connection__idx__lookup.html#af678ceef0a35712b1cef7d65ddc945bd',1,'tcp_tx_data_connection_idx_lookup']]],
  ['src_5fport_17',['src_port',['../structtcp__rx__data__filter__lookup.html#a6a08a76b93ecd278f9ad26cd67374d69',1,'tcp_rx_data_filter_lookup::src_port()'],['../structtcp__tx__data__connection__idx__lookup.html#a5cb08a0f4f269f37c7a1470dade8278b',1,'tcp_tx_data_connection_idx_lookup::src_port()']]],
  ['src_5fport_5fmatch_5fdis_18',['src_port_match_dis',['../structtcp__rx__data__filter__lookup.html#aaa27c9d2f7b3a12b3e850df7dc3b3daa',1,'tcp_rx_data_filter_lookup']]],
  ['st_5fqueues_19',['st_queues',['../structport__info.html#ae20f12afd6974546a7621f4e8ca3110a',1,'port_info']]],
  ['statistics_5freset_20',['statistics_reset',['../structsystem__register__definition.html#a8438a5952a9c28522d9a7804f194972e',1,'system_register_definition']]],
  ['statistics_5freset_5fafter_5fread_21',['statistics_reset_after_read',['../structsystem__register__definition.html#a907f731c88cf1f9a0440efa06fdf0b31',1,'system_register_definition']]],
  ['sw_5fcomp_5frx_5fdma_5fcounter_22',['SW_comp_rx_dma_counter',['../structsoftware__counters.html#a2f55feb809fc35b200a885fc764981b5',1,'software_counters']]],
  ['sw_5fduplication_5freject_5fcounter_23',['SW_duplication_reject_counter',['../structsoftware__counters.html#abb828637578cccce2a65499d0c9b98cf',1,'software_counters']]],
  ['sw_5fekd_5fmatch_5fcounter_24',['SW_ekd_match_counter',['../structsoftware__counters.html#af0296c4384a7c06cf032e7ace5529b9c',1,'software_counters']]],
  ['sw_5fekd_5fpkt1_5fmatched_25',['SW_ekd_pkt1_matched',['../structsoftware__counters.html#a3e231e70cbe0df59cdba00ffbc1647bd',1,'software_counters']]],
  ['sw_5fekd_5fpkt2_5fmatched_26',['SW_ekd_pkt2_matched',['../structsoftware__counters.html#a86cc16923bc3b942b03091b0f8998cb7',1,'software_counters']]],
  ['sw_5ffpga_5fmatcherkeys_27',['SW_fpga_matcherkeys',['../structsoftware__counters.html#ab94879f4efed71aeadaa000fcc9a6862',1,'software_counters']]],
  ['sw_5ffpga_5ftrigger_5fcounter_28',['SW_fpga_trigger_counter',['../structsoftware__counters.html#a7f76fa7885814d7ccee5d467e073e453',1,'software_counters']]],
  ['sw_5ffpga_5ftrigger_5fpre_5fkey_5fcounter_29',['SW_fpga_trigger_pre_key_counter',['../structsoftware__counters.html#aaab302c7ba1c6a8950eaa748a7067e97',1,'software_counters']]],
  ['sw_5finvalid_5fpackets_30',['SW_invalid_packets',['../structsoftware__counters.html#a4350035402125b5f364e5c3e1928d00c',1,'software_counters']]],
  ['sw_5fkernel_5ftcp_5frx_5fdma_5fcounter_31',['SW_kernel_tcp_rx_dma_counter',['../structsoftware__counters.html#a2761d433be9c2a41ec03c381cbc6096f',1,'software_counters']]],
  ['sw_5fkernel_5ftcp_5ftx_5fdma_5fcounter_32',['SW_kernel_tcp_tx_dma_counter',['../structsoftware__counters.html#abbd1181d223f84d852137cb8abe47281',1,'software_counters']]],
  ['sw_5fkernel_5fudp_5frx_5fdma_5fcounter_33',['SW_kernel_udp_rx_dma_counter',['../structsoftware__counters.html#a54bd6bea9a8682565cc513c7618398b6',1,'software_counters']]],
  ['sw_5fkernel_5fudp_5ftx_5fdma_5fcounter_34',['SW_kernel_udp_tx_dma_counter',['../structsoftware__counters.html#ad56fc3a1d849f663efdf3ec1a5e5298c',1,'software_counters']]],
  ['sw_5fkey_5fdyn_5freject_5fcounter_35',['SW_key_dyn_reject_counter',['../structsoftware__counters.html#ae60cdfffa9a1a42cafa39f89d6b77942',1,'software_counters']]],
  ['sw_5fkey_5fen_5freject_5fcounter_36',['SW_key_en_reject_counter',['../structsoftware__counters.html#aa9c41765dbaf68ee26e34a5e0d01f782',1,'software_counters']]],
  ['sw_5fmatcher_5fdropped_37',['SW_matcher_dropped',['../structsoftware__counters.html#a0865a5a0880112997297a24d3bf12de0',1,'software_counters']]],
  ['sw_5fmatcher_5fmatched_38',['SW_matcher_matched',['../structsoftware__counters.html#a2891e18613e7ca45a4e90b27418524e6',1,'software_counters']]],
  ['sw_5fmatcher_5fpacket_39',['SW_matcher_packet',['../structsoftware__counters.html#ad3dfa59791e6ae8afaf5f1779a5eea14',1,'software_counters']]],
  ['sw_5fmatcher_5fpacket_5fdropped_40',['SW_matcher_packet_dropped',['../structsoftware__counters.html#acd51f84f7993eacf2bb2161fb061f893',1,'software_counters']]],
  ['sw_5fmatcherkeys_41',['sw_matcherkeys',['../structheartbeat__statistics__counter.html#a754c15a0666c01140e7560cf57249288',1,'heartbeat_statistics_counter']]],
  ['sw_5fnotf_5frx_5fdma_5fcounter_42',['SW_notf_rx_dma_counter',['../structsoftware__counters.html#ad4fc22b634c0ca4262bc30ad5bd0ce77',1,'software_counters']]],
  ['sw_5fnotification_43',['SW_Notification',['../ekd__util_8h.html#ac204d177822b401a610911536596336e',1,'ekd_util.h']]],
  ['sw_5fnotification_5ffile_44',['SW_notification_file',['../ekd__util_8h.html#a93c587f56bd544ffd5462e04fd4e49d1',1,'ekd_util.h']]],
  ['sw_5frate_5flimit_5freject_5fcounter_45',['SW_rate_limit_reject_counter',['../structsoftware__counters.html#ae84237d7681ad5738597637f9b10b13c',1,'software_counters']]],
  ['sw_5fring_46',['sw_ring',['../structrx__priority__queue.html#a30e8c2b890814b86c8bd734fd50a9969',1,'rx_priority_queue']]],
  ['sw_5frx_5fack_5fcounter_47',['sw_rx_ack_counter',['../structsw__tcp__rx__counters.html#aaf37cd9a0b785bdeb2f131fc795aad05',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5fbmissed_5fcounter_48',['sw_rx_bmissed_counter',['../structsw__tcp__rx__counters.html#aa835fe9b2290d85485fd457418d898ea',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5fcounter_49',['sw_rx_counter',['../structsw__tcp__rx__counters.html#a1941a3b0e4ef3a5d9f8dabc4de4459d8',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5ffin_5fcounter_50',['sw_rx_fin_counter',['../structsw__tcp__rx__counters.html#a71be083cfdeb02be02ea67902fb75d7c',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5fpsh_5fack_5fcounter_51',['sw_rx_psh_ack_counter',['../structsw__tcp__rx__counters.html#a0029529f8b6e2a9e3caa89e3fab32a1b',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5freject_5fcounter_52',['sw_rx_reject_counter',['../structsw__tcp__rx__counters.html#aaad178f283d6b6ecaec3ae5c06ef613a',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5frst_5fcounter_53',['sw_rx_rst_counter',['../structsw__tcp__rx__counters.html#af0ee32dd839f52b41c2252ef8a59ac10',1,'sw_tcp_rx_counters']]],
  ['sw_5frx_5fsyn_5fack_5fcounter_54',['sw_rx_syn_ack_counter',['../structsw__tcp__rx__counters.html#aa2310dec329621fbc6e375472ef3fba0',1,'sw_tcp_rx_counters']]],
  ['sw_5fsw_5fmatcherkeys_55',['SW_sw_matcherkeys',['../structsoftware__counters.html#abe0f912cfe6cc2b6c86836079dd93025',1,'software_counters']]],
  ['sw_5fsw_5ftrigger_5fcounter_56',['SW_sw_trigger_counter',['../structsoftware__counters.html#a031a8ce8367b74687be15d3dcce64930',1,'software_counters']]],
  ['sw_5fsw_5ftrigger_5fpre_5fkey_5fcounter_57',['SW_sw_trigger_pre_key_counter',['../structsoftware__counters.html#a4927d16454387c65f7f055951ecdeb68',1,'software_counters']]],
  ['sw_5fsw_5ftx_5fkey_5fctrl_5fcounter_58',['SW_sw_tx_key_ctrl_counter',['../structsoftware__counters.html#a5a7c1245e3b5006b41c9213b39cc7cc5',1,'software_counters']]],
  ['sw_5fsw_5ftx_5fkey_5fdata_5fcounter_59',['SW_sw_tx_key_data_counter',['../structsoftware__counters.html#a0b741211e4bc795c50d9c92f23bdf1b6',1,'software_counters']]],
  ['sw_5ftcp_5frx_5fdma_5fcounter_60',['SW_tcp_rx_dma_counter',['../structsoftware__counters.html#ad73bc4ca2f6e2b405973b588ba68591f',1,'software_counters']]],
  ['sw_5ftcp_5frx_5fmac_5fcounter_61',['SW_tcp_rx_mac_counter',['../structsoftware__counters.html#a21078c7d220bbf934ae353854a664068',1,'software_counters']]],
  ['sw_5ftcp_5frx_5fmac_5ferr_5fcounter_62',['SW_tcp_rx_mac_err_counter',['../structsoftware__counters.html#a9f17067747e68e379242d0bb98bb105b',1,'software_counters']]],
  ['sw_5ftcp_5ftx_5fdma_5fcounter_63',['SW_tcp_tx_dma_counter',['../structsoftware__counters.html#acd01f85cb28b404647a3008d34fe9caa',1,'software_counters']]],
  ['sw_5ftcp_5ftx_5fmac_5fcounter_64',['SW_tcp_tx_mac_counter',['../structsoftware__counters.html#a271db349eb0a31674b1c7d7e054ff98a',1,'software_counters']]],
  ['sw_5ftrigger_5fcidx_65',['sw_trigger_cidx',['../ekd__api_8h.html#aa5b17ee72c2b789325859a6e268fa8b2',1,'ekd_api.h']]],
  ['sw_5ftrigger_5fcounter_66',['sw_trigger_counter',['../structheartbeat__statistics__counter.html#ae578bfba3df1cc0d82334e70fa6241cf',1,'heartbeat_statistics_counter']]],
  ['sw_5ftrigger_5fpidx_67',['sw_trigger_pidx',['../ekd__api_8h.html#accd83478d0b0a4c0e9833d62f701115d',1,'ekd_api.h']]],
  ['sw_5ftrigger_5fpre_5fkey_5fcounter_68',['sw_trigger_pre_key_counter',['../structheartbeat__statistics__counter.html#ae9e30c826443e2ab290f36a5758dd3c3',1,'heartbeat_statistics_counter']]],
  ['sw_5ftx_5fack_5fcounter_69',['sw_tx_ack_counter',['../structsw__tcp__tx__counters.html#a26933e7c63bc82b76ea12de90e62c127',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5fcounter_70',['sw_tx_counter',['../structsw__tcp__tx__counters.html#add052f91e146e06b16dc917dd391f9d7',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5ffin_5fcounter_71',['sw_tx_fin_counter',['../structsw__tcp__tx__counters.html#a254af84df3ebe00f40fa1208e7f45d6d',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5fkey_5fctrl_5fcounter_72',['sw_tx_key_ctrl_counter',['../structheartbeat__statistics__counter.html#a17409d590044ecd1f129effb62de17f3',1,'heartbeat_statistics_counter']]],
  ['sw_5ftx_5fkey_5fdata_5fcounter_73',['sw_tx_key_data_counter',['../structheartbeat__statistics__counter.html#adabeadc38561d68372f2f75f4f8c7987',1,'heartbeat_statistics_counter']]],
  ['sw_5ftx_5fmodified_5fdata_74',['sw_tx_modified_data',['../structsw__tcp__tx__counters.html#a42f0a205aa40a8f6252bc138374f6c70',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5frexmit_5fcounter_75',['sw_tx_rexmit_counter',['../structsw__tcp__tx__counters.html#a0819c740a7b397a4103f28ee7e2c96e0',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5fshort_5fdata_76',['sw_tx_short_data',['../structsw__tcp__tx__counters.html#a3cb89a67292e4497fb77efd699055b30',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5fsyn_5fcounter_77',['sw_tx_syn_counter',['../structsw__tcp__tx__counters.html#a01151ae30501d3f35092286c9d7bbd25',1,'sw_tcp_tx_counters']]],
  ['sw_5ftx_5funmodified_5fdata_78',['sw_tx_unmodified_data',['../structsw__tcp__tx__counters.html#a363c34dd605b3da226dd069775cd59f0',1,'sw_tcp_tx_counters']]],
  ['sw_5fudp_5frx_5fdma_5fcounter_79',['SW_udp_rx_dma_counter',['../structsoftware__counters.html#ac033c2bb73928fc5eca0044cbe83ddd9',1,'software_counters']]],
  ['sw_5fudp_5frx_5fmac_5fcounter_80',['SW_udp_rx_mac_counter',['../structsoftware__counters.html#a8ac76d54fff4dc6a7fe7bdc34a4763be',1,'software_counters']]],
  ['sw_5fudp_5frx_5fmac_5ferr_5fcounter_81',['SW_udp_rx_mac_err_counter',['../structsoftware__counters.html#a010ffa3221a49dcdda1477c6390e1f4a',1,'software_counters']]],
  ['sw_5fudp_5ftx_5fdma_5fcounter_82',['SW_udp_tx_dma_counter',['../structsoftware__counters.html#ab4f8b609997e7c79f7b33906882ef39d',1,'software_counters']]],
  ['sw_5fudp_5ftx_5fmac_5fcounter_83',['SW_udp_tx_mac_counter',['../structsoftware__counters.html#a1538287064a06c29dfbb69af15bf95aa',1,'software_counters']]],
  ['sw_5futcp_5frx_5fdma_5fcounter_84',['SW_utcp_rx_dma_counter',['../structsoftware__counters.html#aaaf979b95ff253e55b33d57e5d1974ea',1,'software_counters']]],
  ['sw_5futcp_5frx_5fmac_5fcounter_85',['SW_utcp_rx_mac_counter',['../structsoftware__counters.html#a4a33511754956708f2cc485decaed1b2',1,'software_counters']]],
  ['sw_5futcp_5frx_5fmac_5ferr_5fcounter_86',['SW_utcp_rx_mac_err_counter',['../structsoftware__counters.html#a5e2cef3fa9fe0b5a131564514bfacd59',1,'software_counters']]],
  ['sw_5fuudp_5frx_5fmac_5fcounter_87',['SW_uudp_rx_mac_counter',['../structsoftware__counters.html#a4beb08d722b21a2fe538c8e900da70fa',1,'software_counters']]],
  ['sw_5fuudp_5frx_5fmac_5ferr_5fcounter_88',['SW_uudp_rx_mac_err_counter',['../structsoftware__counters.html#a58942fae0d6b6e197ad959240eba6707',1,'software_counters']]],
  ['sysin_5ft_89',['sysin_t',['../ekd__util_8h.html#af578a5bf22ca5c27906394d33753b581',1,'ekd_util.h']]],
  ['system_5fregister_90',['system_register',['../ekd__util_8h.html#abb121c03878b27014d3227dfcb059605',1,'ekd_util.h']]],
  ['system_5fregister_5fdefinition_91',['system_register_definition',['../ekd__util_8h.html#a1e15326da50f8ab6019e8cbf79dede47',1,'ekd_util.h']]]
];
