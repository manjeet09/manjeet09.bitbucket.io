var searchData=
[
  ['baseaddr_0',['baseaddr',['../ekd__api_8h.html#af8882f2061b9e9cbe14135d11971e89b',1,'ekd_api.h']]],
  ['bind_5ffpga_5fcard_1',['bind_fpga_card',['../ekd__api_8h.html#ae93005309dfdc9ae13265c41637636e8',1,'bind_fpga_card():&#160;ekd_api.c'],['../ekd__util_8h.html#ae93005309dfdc9ae13265c41637636e8',1,'bind_fpga_card():&#160;ekd_api.c'],['../ekd__api_8c.html#ae93005309dfdc9ae13265c41637636e8',1,'bind_fpga_card():&#160;ekd_api.c']]],
  ['bits_5fcount_2',['bits_count',['../cat_8c.html#a9771609725467f203e0eb791194f54fa',1,'cat.c']]],
  ['bits_5fper_5fhex_3',['BITS_PER_HEX',['../cat_8c.html#a05b4933ae7b0722eab4fafa9eba15827',1,'cat.c']]],
  ['buff_5fsize_4',['buff_size',['../structport__info.html#a77f5903a079f37bc22ca6d727f38979f',1,'port_info']]],
  ['buffer_5fsize_5',['buffer_size',['../structqbit__rxq__info.html#a005d3954d6c633aefe8e47104e113b7f',1,'qbit_rxq_info::buffer_size()'],['../structqbit__txq__info.html#a9b054bc0121baf3095995e04431f7014',1,'qbit_txq_info::buffer_size()']]],
  ['burst_5fsize_6',['BURST_SIZE',['../ekd__util_8h.html#a703e079cb598d615c750d24ec7432617',1,'ekd_util.h']]],
  ['bypass_5fbar_5fidx_7',['bypass_bar_idx',['../structport__info.html#a15c11ae026ea0a2c1d22a86a0377e051',1,'port_info']]]
];
