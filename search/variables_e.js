var searchData=
[
  ['name_0',['name',['../structqbit__rxq__info.html#a2827efec2be97d79ea4b5ab05c784a14',1,'qbit_rxq_info::name()'],['../structqbit__txq__info.html#a757d4b42a0e8a86419bceddab01df03f',1,'qbit_txq_info::name()']]],
  ['nb_5fdescs_1',['nb_descs',['../structport__info.html#a816eca0b262eac5102741f6f17df4cd0',1,'port_info']]],
  ['nb_5fkni_2',['nb_kni',['../structkni__port__params.html#a4437e8de9feac4b9e7723b0be99f9d9d',1,'kni_port_params']]],
  ['new_5ftrigger_5fkey_5fdata_3',['new_trigger_key_data',['../ekd__diagnostic_8c.html#a9d2633f2a70d065db0eae878d9e083a2',1,'ekd_diagnostic.c']]],
  ['next_4',['next',['../structpkt__queue__data.html#a055fd2ec223c1e806fb63f2ab749e3d0',1,'pkt_queue_data::next()'],['../structxmit__pio__queue__data.html#a571caacc201930a1783406ea880346ed',1,'xmit_pio_queue_data::next()'],['../structxmit__queue__data.html#a8dc99b04e12b80796e75e148f528030a',1,'xmit_queue_data::next()'],['../structxmit__queue__data__mbuf.html#a3eff4d371f4d773abee1a2b91483bb1a',1,'xmit_queue_data_mbuf::next()']]],
  ['notf_5frx_5fdma_5fcounter_5',['notf_rx_dma_counter',['../structheartbeat__statistics__counter.html#a7e13d150c9d8eab11987a04250188328',1,'heartbeat_statistics_counter']]],
  ['notif_5fin_6',['notif_in',['../sample__app_8c.html#a22962ed0fcfdfe51050e98698497affb',1,'sample_app.c']]],
  ['notification_5ftype_7',['notification_type',['../structrx__notification__dma__header.html#a6c124336a5c49089a8d1eb75946811ef',1,'rx_notification_dma_header']]],
  ['notificationrx_5ft_8',['notificationrx_t',['../ekd__util_8h.html#a2c6ca7ab648c5249633d8bce390eaf96',1,'ekd_util.h']]],
  ['num_5fports_9',['num_ports',['../sample__app_8c.html#a9ab345324d5fde83b02bc16f817ae040',1,'num_ports():&#160;sample_app.c'],['../ekd__api_8h.html#a9ab345324d5fde83b02bc16f817ae040',1,'num_ports():&#160;ekd_api.h']]],
  ['num_5fqueues_10',['num_queues',['../structport__info.html#abba979202e1baff37876b8e6f24a3fd8',1,'port_info']]]
];
