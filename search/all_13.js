var searchData=
[
  ['rangeerror_0',['RANGEERROR',['../ekd__api_8h.html#a30d2a2a40fc1df453692db7701a4d36b',1,'ekd_api.h']]],
  ['rate_5flimit_1',['rate_limit',['../structtcp__tx__data__connection__idx__lookup.html#a51733500c56de36babbdd514ac824cc8',1,'tcp_tx_data_connection_idx_lookup']]],
  ['rate_5flimit_5freject_5fcounter_2',['rate_limit_reject_counter',['../structheartbeat__statistics__counter.html#ac5416e1a5ad022f5d3611a5b3ec7617e',1,'heartbeat_statistics_counter']]],
  ['read128_3',['read128',['../ekd__util_8c.html#ac25ec97f5636394fbc64a32a47fb11f0',1,'ekd_util.c']]],
  ['read256_4',['read256',['../ekd__util_8c.html#a125630da8c8d6e52921bf16cac949cba',1,'ekd_util.c']]],
  ['read512_5',['read512',['../ekd__util_8c.html#a83358c14f0ca228583b26d7f3ad15728',1,'ekd_util.c']]],
  ['read_5fhex_6',['read_hex',['../ekd__util_8c.html#acee5a9947abe93906a4cf5243935e65e',1,'read_hex(void):&#160;ekd_util.c'],['../ekd__util_8h.html#acee5a9947abe93906a4cf5243935e65e',1,'read_hex(void):&#160;ekd_util.c']]],
  ['read_5fint_7',['read_int',['../ekd__util_8c.html#ace5d3a3656c76314ca0216805093797d',1,'read_int(void):&#160;ekd_util.c'],['../ekd__util_8h.html#ace5d3a3656c76314ca0216805093797d',1,'read_int(void):&#160;ekd_util.c']]],
  ['read_5fstr_8',['read_str',['../ekd__util_8c.html#a5757e45e2a7640dc186d17c039092284',1,'read_str(void):&#160;ekd_util.c'],['../ekd__util_8h.html#a5757e45e2a7640dc186d17c039092284',1,'read_str(void):&#160;ekd_util.c']]],
  ['read_5fsystem_5fregisters_9',['read_system_registers',['../ekd__api_8c.html#a5ce96f38a702b6965f8eaf61f6e3cf5e',1,'read_system_registers(uint32_t port_id, struct system_register_definition *get_system_register, uint32_t *extra_info):&#160;ekd_api.c'],['../ekd__api_8h.html#a5ce96f38a702b6965f8eaf61f6e3cf5e',1,'read_system_registers(uint32_t port_id, struct system_register_definition *get_system_register, uint32_t *extra_info):&#160;ekd_api.c']]],
  ['rem_5frx_5fdma_5ftemp_10',['rem_rx_dma_temp',['../ekd__util_8c.html#a537225ec36935bf5ef79c99b240ec65d',1,'ekd_util.c']]],
  ['remove_5frx_5fdma_5fheader_11',['remove_rx_dma_header',['../ekd__util_8h.html#a02a032dc178614e44668f51aaa2c0a90',1,'remove_rx_dma_header(u_char *packet, struct pcap_pkthdr *pkthdr, struct rx_dma_header *rx_dmahdr):&#160;ekd_util.c'],['../ekd__util_8c.html#a02a032dc178614e44668f51aaa2c0a90',1,'remove_rx_dma_header(u_char *packet, struct pcap_pkthdr *pkthdr, struct rx_dma_header *rx_dmahdr):&#160;ekd_util.c']]],
  ['reserved_12',['reserved',['../structarp__header.html#a2ccdfce94becfcae870df7541c647c85',1,'arp_header::reserved()'],['../structtcp__tx__dma__header.html#a41c0eda7df0e43a20085dc1027811d62',1,'tcp_tx_dma_header::reserved()'],['../structmatcher__block__per__ekd__info.html#acc88731313d7cf6c44efb5f8f02c6c79',1,'matcher_block_per_ekd_info::reserved()'],['../structtx__userspace__type.html#a365509f6e813efccf0ef049d987e8dae',1,'tx_userspace_type::reserved()'],['../structrx__userspace__type.html#ae2775a8273940a51440985d60a52d6e0',1,'rx_userspace_type::reserved()'],['../structtx__channel.html#a3772ee7c21428c6f270fc8b0cc6f3f9f',1,'tx_channel::reserved()'],['../structrx__channel.html#a8520fe23e281472afb640faa1059febc',1,'rx_channel::reserved()']]],
  ['reserved_5f1_13',['reserved_1',['../structudp__rx__data__filter__lookup.html#a8bcfde1808aed75664ef2e6d1283a7ab',1,'udp_rx_data_filter_lookup::reserved_1()'],['../structsystem__register__definition.html#aae0c33de5ada8560ff9d1ebca4eddf85',1,'system_register_definition::reserved_1()'],['../structtrigger__notification.html#aaf62d707a9d3a0e33139a74ce6287a23',1,'trigger_notification::reserved_1()'],['../structmatcher__notification.html#af878326405dfb4170495a9200f08c946',1,'matcher_notification::reserved_1()'],['../structrx__notification__dma__header.html#a436284d008a99f94aa0f6a00d677cb18',1,'rx_notification_dma_header::reserved_1()'],['../structtrigger__key__rule__check__config.html#ac5486c06d88b79c69c5000b8154f9b0b',1,'trigger_key_rule_check_config::reserved_1()'],['../structtcp__tx__data__connection__idx__lookup.html#ac21ae21ab230562d4e2ee5d8de236871',1,'tcp_tx_data_connection_idx_lookup::reserved_1()'],['../structtrigger__key__info.html#a161e6ac9cdcc692d7abf07b6cc87ca9a',1,'trigger_key_info::reserved_1()'],['../structmatcher__info.html#adcf010eca2c85e517f020101ae3be659',1,'matcher_info::reserved_1()'],['../structmatcher__block__per__ekd__info.html#a091985fc8ee67465ede2f0c67d9dde22',1,'matcher_block_per_ekd_info::reserved_1()'],['../structtcp__tx__dma__header.html#af2b1d904acbecf9e6f2240419e6fcb5f',1,'tcp_tx_dma_header::reserved_1()'],['../structtcp__rx__data__filter__lookup.html#ade7912854a7cfe68b07442958e28778f',1,'tcp_rx_data_filter_lookup::reserved_1()']]],
  ['reserved_5f2_14',['reserved_2',['../structtcp__tx__data__connection__idx__lookup.html#a09fd948a71fc575cf99b432cf6ce7907',1,'tcp_tx_data_connection_idx_lookup::reserved_2()'],['../structrx__dma__header.html#a89bb7d76fa86016cd9b9f6af8f9154a6',1,'rx_dma_header::reserved_2()'],['../structtcp__tx__dma__header.html#afe22f5378995ee9bc5de4cb97a7ef3e0',1,'tcp_tx_dma_header::reserved_2()'],['../structmatcher__info.html#a6b578495a9c9457124d409c14212d923',1,'matcher_info::reserved_2()'],['../structtrigger__key__info.html#a66d0df3525834c56b39ef58582bea8c3',1,'trigger_key_info::reserved_2()'],['../structtcp__rx__data__filter__lookup.html#a196b4b8a1bf0b85a302c05a362be44d1',1,'tcp_rx_data_filter_lookup::reserved_2()'],['../structtcp__connection__status.html#adc1982b7e518f1686aa4108d0e056e92',1,'tcp_connection_status::reserved_2()'],['../structtrigger__key__rule__check__config.html#aa44c8608ad668730b3811b8f2de7fbde',1,'trigger_key_rule_check_config::reserved_2()'],['../structtrigger__notification.html#abf22b6c9b5e9688ced7f05348f0c7b72',1,'trigger_notification::reserved_2()'],['../structsystem__register__definition.html#afdae9eb9b4472040f44c3e36b07c0c39',1,'system_register_definition::reserved_2()'],['../structudp__rx__data__filter__lookup.html#a1d2ed45a9ee87c104e47dfd36bb54a93',1,'udp_rx_data_filter_lookup::reserved_2()']]],
  ['reserved_5f3_15',['reserved_3',['../structtcp__tx__dma__header.html#a8ad6d26f3286915d70b7ee9737a74472',1,'tcp_tx_dma_header::reserved_3()'],['../structrx__dma__header.html#aedca085f4a4ab987886e706ff1b5ba72',1,'rx_dma_header::reserved_3()'],['../structsystem__register__definition.html#afca9b57198f746c7a3a6844a7cf7caf1',1,'system_register_definition::reserved_3()'],['../structtrigger__key__info.html#a38583fd8c0dafb8b2ef747f265d65057',1,'trigger_key_info::reserved_3()'],['../structmatcher__info.html#a6cf8ffe4f24c370f70c14b5ae6921f6e',1,'matcher_info::reserved_3()']]],
  ['reserved_5f31_16',['reserved_31',['../structrx__dma__header.html#adb95c12182c6a330ad3ffcc13d9f6bef',1,'rx_dma_header']]],
  ['reserved_5f32_17',['reserved_32',['../structrx__dma__header.html#a7d9223533424804c2fa5b9fa84e2e799',1,'rx_dma_header']]],
  ['reserved_5f4_18',['reserved_4',['../structsystem__register__definition.html#a4bae561d1e39f64706720efc16ae4f4c',1,'system_register_definition::reserved_4()'],['../structmatcher__info.html#a69af8e973beb293d4fa3e24361ecff6e',1,'matcher_info::reserved_4()'],['../structtcp__tx__dma__header.html#a5315add6b7658b7f063f06ef2c76fbe8',1,'tcp_tx_dma_header::reserved_4()']]],
  ['reserved_5f5_19',['reserved_5',['../structsystem__register__definition.html#ae228c3371fe4aabb45f8a760f8161f7a',1,'system_register_definition']]],
  ['reserved_5f6_20',['reserved_6',['../structsystem__register__definition.html#aac627cfcbf14924e724cb5926ea0465f',1,'system_register_definition']]],
  ['reserved_5f7_21',['reserved_7',['../structsystem__register__definition.html#a81d85db754d9d37ef0a5c86249dd7b72',1,'system_register_definition']]],
  ['reserved_5f9_22',['reserved_9',['../structsystem__register__definition.html#a06eb881160c559ddd76f656107fc790e',1,'system_register_definition']]],
  ['reserved_5fextra_23',['reserved_extra',['../structmatcher__block__per__ekd__info.html#a643437d9e444b7ba08c117ba83374c30',1,'matcher_block_per_ekd_info']]],
  ['resetcontrol_5foffset_24',['RESETCONTROL_OFFSET',['../ekd__api_8h.html#afb79f6b871148519ba32461c6204686d',1,'ekd_api.h']]],
  ['resurved_5f1_25',['resurved_1',['../structsystem__register.html#ae16a3849bbf3036dbf2760b0c438d871',1,'system_register']]],
  ['resurved_5f2_26',['resurved_2',['../structsystem__register.html#ab6d3fe8572265c1995674b6aa9b089c0',1,'system_register']]],
  ['resurved_5f3_27',['resurved_3',['../structsystem__register.html#a9e4ba401c9ce576c919be8ca8666612e',1,'system_register']]],
  ['resurved_5f4_28',['resurved_4',['../structsystem__register.html#a5db8040684f23fdac7d1b4eadf5b5aaf',1,'system_register']]],
  ['resurved_5f5_29',['resurved_5',['../structsystem__register.html#aa3e6ebb3412f93cf5b7f5d6cff5e4d08',1,'system_register']]],
  ['rexmit_5fqueue_30',['rexmit_queue',['../structtcp__params.html#a7bb03552b192a9d790b42e22b1559df4',1,'tcp_params']]],
  ['rexmit_5fqueue_5flock_31',['rexmit_queue_lock',['../structtcp__params.html#a9e703e117f166c0c72e86200d5f17490',1,'tcp_params']]],
  ['rexmit_5ftcp_5fpio_5fcntr_32',['rexmit_tcp_pio_cntr',['../structheartbeat__statistics__counter.html#ae96cbf94e76ca97d0293e3e3184240b1',1,'heartbeat_statistics_counter']]],
  ['rexmit_5fudp_5fpio_5fcntr_33',['rexmit_udp_pio_cntr',['../structheartbeat__statistics__counter.html#a5b485a9a1dd03212e82e2584ed2d751f',1,'heartbeat_statistics_counter']]],
  ['rhs_5fdata_5fp_34',['rhs_data_p',['../structtrigger__key__data__set.html#aa1bc985222c3b25119a2214909659b34',1,'trigger_key_data_set::rhs_data_p()'],['../structmatcher__info.html#a2f6e24b4418433c341de2888ea157ae7',1,'matcher_info::rhs_data_p()']]],
  ['rhs_5fdata_5fp_5fq_35',['rhs_data_p_q',['../structtrigger__key__data__set.html#a351145923e7e07d6b25ff769f6792232',1,'trigger_key_data_set::rhs_data_p_q()'],['../structmatcher__info.html#a0bd8764cc581440f14e4ecc4ad28f507',1,'matcher_info::rhs_data_p_q()']]],
  ['rhs_5fdata_5fp_5fq_5fselect_36',['rhs_data_p_q_select',['../structmatcher__info.html#a6ca8256264d6d6f160e61bfbb59e68da',1,'matcher_info::rhs_data_p_q_select()'],['../structtrigger__key__data__set.html#a2cd8927ff1cc1772bb2be956eae83ca0',1,'trigger_key_data_set::rhs_data_p_q_select()']]],
  ['rhs_5fdata_5fq_37',['rhs_data_q',['../structtrigger__key__data__set.html#abdb732728fcb4a487bd87d1d2af41bf4',1,'trigger_key_data_set::rhs_data_q()'],['../structmatcher__info.html#af694d0dec47fb716a55923b3634b5d16',1,'matcher_info::rhs_data_q()']]],
  ['rhs_5fdata_5ft_5fo_38',['rhs_data_t_o',['../structtrigger__key__data__set.html#adf1af11215466983ecf309a6ba662f28',1,'trigger_key_data_set::rhs_data_t_o()'],['../structmatcher__info.html#a26ce97554c6edf07d8cef3ad84903665',1,'matcher_info::rhs_data_t_o()']]],
  ['rhs_5fdata_5ft_5fo_5fselect_39',['rhs_data_t_o_select',['../structtrigger__key__data__set.html#aca99165cd0b88f4641a92e2732d822ea',1,'trigger_key_data_set::rhs_data_t_o_select()'],['../structmatcher__info.html#abf2e72f1680c201b5dda2fd833b9acad',1,'matcher_info::rhs_data_t_o_select()']]],
  ['rhs_5fpkt_5ftype_5f1_40',['rhs_pkt_type_1',['../structmatcher__info.html#abe22105209dc5e6a5aba29887317d296',1,'matcher_info::rhs_pkt_type_1()'],['../structtrigger__key__data__set.html#a000877cbfbcdd2ec44734e6c32a25b40',1,'trigger_key_data_set::rhs_pkt_type_1()']]],
  ['rhs_5fpkt_5ftype_5f2_41',['rhs_pkt_type_2',['../structmatcher__info.html#aaaad14c932deb07bb21ea77c483cfc75',1,'matcher_info::rhs_pkt_type_2()'],['../structtrigger__key__data__set.html#a8feb247016ac0bdaa1911324869659c3',1,'trigger_key_data_set::rhs_pkt_type_2()']]],
  ['ring_5fsize_42',['ring_size',['../structqbit__txq__info.html#ae66591d928c1ed3158b939aa049d4a37',1,'qbit_txq_info::ring_size()'],['../structqbit__rxq__info.html#a5fbe38146af6b765bc13fc88e417365d',1,'qbit_rxq_info::ring_size()']]],
  ['rseq_43',['Rseq',['../structtcp__option__sack__combo__h.html#a1ae3f5d0fd47e7877ba45eea32a5fec0',1,'tcp_option_sack_combo_h']]],
  ['rsvd_5fcntr_44',['rsvd_cntr',['../structheartbeat__statistics__counter.html#ac30547e007444ccb1b203646b04215e0',1,'heartbeat_statistics_counter']]],
  ['rte_5fmax_5fethports_45',['RTE_MAX_ETHPORTS',['../ekd__api_8h.html#ae847c8dd2de8a1751c4e99e6b0476a35',1,'ekd_api.h']]],
  ['rule_5fcheck_5fid_46',['rule_check_id',['../structtrigger__notification.html#ac857939a2a40750de2eb4d151815094a',1,'trigger_notification']]],
  ['rule_5fcheck_5fstatus_47',['rule_check_status',['../structtrigger__notification.html#a6e952a48fc9fbf4f249369de8315b594',1,'trigger_notification']]],
  ['rule_5fcheck_5fversion_48',['rule_check_version',['../structtrigger__notification.html#a830a2596afa38484dea06137bc8c5128',1,'trigger_notification']]],
  ['rx_5fchannel_49',['rx_channel',['../structrx__channel.html',1,'rx_channel'],['../ekd__util_8h.html#a05da8945eb488f12705136517f621bc4',1,'rx_channel():&#160;ekd_util.h']]],
  ['rx_5fclient_50',['rx_client',['../ekd__util_8h.html#ad80c655d9f685a782b4609bfe70247c4',1,'ekd_util.h']]],
  ['rx_5fdma_5fapp_5fnotf_5fqid_51',['RX_DMA_APP_NOTF_QID',['../ekd__util_8h.html#a6ecc935842dc8b21b83a98148ee8f5e1',1,'ekd_util.h']]],
  ['rx_5fdma_5fapp_5ftcp_5fqid_52',['RX_DMA_APP_TCP_QID',['../ekd__util_8h.html#a5b30b6d72909dcd70bf3569bae4e560f',1,'ekd_util.h']]],
  ['rx_5fdma_5fapp_5fudp_5fqid_53',['RX_DMA_APP_UDP_QID',['../ekd__util_8h.html#aa0dae148d0839a69f430423e08e500a6',1,'ekd_util.h']]],
  ['rx_5fdma_5fapp_5futcp_5fqid_54',['RX_DMA_APP_UTCP_QID',['../ekd__util_8h.html#a0adf0df596c3ffefd4513d0ba27ed648',1,'ekd_util.h']]],
  ['rx_5fdma_5fhdr_5flen_55',['RX_DMA_HDR_LEN',['../ekd__net_8h.html#a4ea9546cc55fb52b13ab5c569085fd08',1,'ekd_net.h']]],
  ['rx_5fdma_5fhdr_5flen_56',['rx_dma_hdr_len',['../ekd__tcp__api_8h.html#aae435fc865659008903cf5f9162a7e95',1,'ekd_tcp_api.h']]],
  ['rx_5fdma_5fheader_57',['rx_dma_header',['../structrx__dma__header.html',1,'']]],
  ['rx_5fdma_5fkernel_5fqid_58',['RX_DMA_KERNEL_QID',['../ekd__util_8h.html#a10b846e8a1b3348388f5f678346001bc',1,'ekd_util.h']]],
  ['rx_5fdropped_59',['rx_dropped',['../structkni__interface__stats.html#af224580e3fbe532cac49c42aa917fd4f',1,'kni_interface_stats']]],
  ['rx_5fglobal_60',['rx_global',['../sample__app_8c.html#a26fa2d389a9bc273a13acb2ca6dcb738',1,'sample_app.c']]],
  ['rx_5fglobal_5fcoreid_61',['RX_GLOBAL_COREID',['../ekd__util_8h.html#a8a0ca4dfef185e50c26500fd3cefeee8',1,'ekd_util.h']]],
  ['rx_5fglobal_5fthread_62',['rx_global_thread',['../ekd__util_8h.html#a1b04aff428c91db5c981b34de5584e5a',1,'ekd_util.h']]],
  ['rx_5fkernel_63',['rx_kernel',['../structrx__channel.html#a6391864aa6bf9e5c56dfdaf5f17dc507',1,'rx_channel']]],
  ['rx_5fkernel_5fdebug_5fpcap_64',['RX_KERNEL_DEBUG_PCAP',['../ekd__api_8h.html#a5783dcde5e2058337963bcf967fa2d89',1,'ekd_api.h']]],
  ['rx_5fkni_5ft_65',['rx_kni_t',['../ekd__util_8h.html#a80e2b3ff2cf331fd63ee29c5274cf39c',1,'ekd_util.h']]],
  ['rx_5fnotification_66',['rx_notification',['../structrx__userspace__type.html#a6d437b851ccbf3abe3ae39c5ff4ee562',1,'rx_userspace_type']]],
  ['rx_5fnotification_5fdma_5fheader_67',['rx_notification_dma_header',['../structrx__notification__dma__header.html',1,'']]],
  ['rx_5fpackets_68',['rx_packets',['../structkni__interface__stats.html#a005a7364f9c944b85cc0e55f2606e137',1,'kni_interface_stats']]],
  ['rx_5fpio_69',['rx_pio',['../ekd__api_8h.html#a53cd890d0e32f2ae9def692ac333199d',1,'rx_pio(int qid, uint8_t **pkt):&#160;ekd_api.c'],['../ekd__api_8c.html#a53cd890d0e32f2ae9def692ac333199d',1,'rx_pio(int qid, uint8_t **pkt):&#160;ekd_api.c']]],
  ['rx_5fpio_5fbase_70',['RX_PIO_BASE',['../ekd__registers_8h.html#a30c8f3ac6e87d89469920fd54573f5b9',1,'ekd_registers.h']]],
  ['rx_5fpio_5fbuffer_5fsize_71',['RX_PIO_BUFFER_SIZE',['../ekd__api_8h.html#acf27938b9fcb5bf338237cf221a6f6fb',1,'ekd_api.h']]],
  ['rx_5fpio_5fen_5fkni_72',['RX_PIO_EN_KNI',['../ekd__kni_8c.html#adb0ae7f79103d08d9b717a0c090c1e10',1,'ekd_kni.c']]],
  ['rx_5fpio_5flseqnum_73',['RX_PIO_LSEQNUM',['../ekd__api_8h.html#ad3d8655ae861fd9b9e178a2c63831761',1,'ekd_api.h']]],
  ['rx_5fpio_5fq_5fsize_74',['RX_PIO_Q_SIZE',['../ekd__api_8h.html#a96c02ae1122e1c23a85e27e249f3ec64',1,'ekd_api.h']]],
  ['rx_5fpio_5ftcp_75',['RX_PIO_TCP',['../ekd__tcp__api_8h.html#a405d0b644f7ba288390a18341a119ba9',1,'ekd_tcp_api.h']]],
  ['rx_5fpriority_5fqueue_76',['rx_priority_queue',['../structrx__priority__queue.html',1,'']]],
  ['rx_5fq_5ftcp_77',['rx_q_tcp',['../ekd__util_8h.html#ab1ea91bcea93310ca42ee2ff26e3e012',1,'ekd_util.h']]],
  ['rx_5fq_5fudp_78',['rx_q_udp',['../ekd__util_8h.html#a3093aacbe44adefecdd0d8c1dfdda741',1,'ekd_util.h']]],
  ['rx_5fring_5fsize_79',['RX_RING_SIZE',['../ekd__util_8h.html#afcfb8ac36c733187de151348eb50ef01',1,'ekd_util.h']]],
  ['rx_5fserver_80',['rx_server',['../ekd__util_8h.html#a8284748f352252c7185555dc70117af6',1,'ekd_util.h']]],
  ['rx_5ftcp_81',['rx_tcp',['../structrx__channel.html#ac388fb0967d44794d072d3c50302d255',1,'rx_channel']]],
  ['rx_5ftcp_5fconnection_5fid_82',['rx_tcp_connection_id',['../structrx__userspace__type.html#a7746acb78e4eac1c11f3b115e8ad100e',1,'rx_userspace_type']]],
  ['rx_5ftcp_5fconnection_5fid_5floopback_83',['rx_tcp_connection_id_loopback',['../structrx__userspace__type.html#a12cccf542b0d3c41832f586bcf72f5a8',1,'rx_userspace_type']]],
  ['rx_5ftcp_5fdebug_5fpcap_84',['RX_TCP_DEBUG_PCAP',['../ekd__api_8h.html#a12297dced527301ee99d3af7dae9d92e',1,'ekd_api.h']]],
  ['rx_5ftcp_5floopback_85',['rx_tcp_loopback',['../structrx__channel.html#a9d9bb53aac888fc3b89b4e5129898aca',1,'rx_channel']]],
  ['rx_5fudp_86',['rx_udp',['../structrx__channel.html#a7dde42fef4380ae3c404ad98755f6e6d',1,'rx_channel']]],
  ['rx_5fudp_5fconnection_5fid_87',['rx_udp_connection_id',['../structrx__userspace__type.html#a4c0d8d028c8e63e5415fa712eec0b491',1,'rx_userspace_type']]],
  ['rx_5fuserspace_5ftype_88',['rx_userspace_type',['../structrx__userspace__type.html',1,'']]],
  ['rx_5futcp_5fdebug_5fpcap_89',['RX_UTCP_DEBUG_PCAP',['../ekd__api_8h.html#ab03c7b2df9b8719124e6ad4d4ba6e3eb',1,'ekd_api.h']]],
  ['rxdma_5fq_5fkernel_90',['rxdma_q_kernel',['../ekd__util_8h.html#a7f3790999607519e45d01ad4ad3370ca',1,'ekd_util.h']]],
  ['rxdma_5fq_5ftcp_91',['rxdma_q_tcp',['../ekd__util_8h.html#a677cd4aa97d5af7916cb60d34228e2ca',1,'ekd_util.h']]],
  ['rxdma_5fq_5fudp_92',['rxdma_q_udp',['../ekd__util_8h.html#afc1ae08363aff43e71c5690e3ed77d70',1,'ekd_util.h']]],
  ['rxdma_5fq_5futcp_93',['rxdma_q_utcp',['../ekd__util_8h.html#af4e88bc1e1865efdce58ea59ea62d243',1,'ekd_util.h']]]
];
