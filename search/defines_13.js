var searchData=
[
  ['udp_5fhdrlen_0',['UDP_HDRLEN',['../ekd__net_8h.html#a55758b7ab4155434bb2a1d1bb706ebd7',1,'ekd_net.h']]],
  ['udp_5frx_5fcoreid_1',['UDP_RX_COREID',['../ekd__util_8h.html#aa48f063f7c24c8c590abcd7b3f1cc3ec',1,'ekd_util.h']]],
  ['udp_5frx_5fdata_5ffilter_5fbase_2',['UDP_RX_DATA_FILTER_BASE',['../ekd__registers_8h.html#aefe7f86c9c092fb64636c2baa4c69361',1,'ekd_registers.h']]],
  ['udp_5frx_5fq_3',['UDP_RX_Q',['../ekd__util_8h.html#a84e3476bec909976a2af64e46f99bca9',1,'ekd_util.h']]],
  ['udp_5ftx_5fcoreid_4',['UDP_TX_COREID',['../ekd__util_8h.html#a19179a8b37b58f99228a8097ece33cc2',1,'ekd_util.h']]],
  ['udp_5ftx_5fq_5',['UDP_TX_Q',['../ekd__util_8h.html#a5f4f0938dfbd521dd4778e1e9436bb91',1,'ekd_util.h']]],
  ['unmodified_5ftcp_5fmode_6',['UNMODIFIED_TCP_MODE',['../ekd__tcp__api_8h.html#aef4e7622bc32d93f3210635268e8ff45',1,'ekd_tcp_api.h']]],
  ['utcp_5frx_5fcoreid_7',['UTCP_RX_COREID',['../ekd__util_8h.html#a32547c3f97ecce64f65a8b759c2cb548',1,'ekd_util.h']]],
  ['utcp_5frx_5fq_8',['UTCP_RX_Q',['../ekd__util_8h.html#aececa42feb946cf5c146511a970e06f4',1,'ekd_util.h']]],
  ['utcp_5ftx_5fq_9',['UTCP_TX_Q',['../ekd__util_8h.html#a15add2bc6b87626a35c93f132f0b9614',1,'ekd_util.h']]]
];
