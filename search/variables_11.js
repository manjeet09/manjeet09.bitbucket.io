var searchData=
[
  ['qbit_5fdev_0',['qbit_dev',['../ekd__util_8h.html#ad97b0f156d88ec6fc339215d5fdafa4a',1,'ekd_util.h']]],
  ['qbit_5frxq_1',['qbit_rxq',['../ekd__util_8h.html#a0d5faa4846b26f5cc8c7ee121887259f',1,'ekd_util.h']]],
  ['qbit_5ftxq_2',['qbit_txq',['../ekd__util_8h.html#ab74195b54d348b1f4abfdbb1fb2543cf',1,'ekd_util.h']]],
  ['qid_3',['qid',['../structqbit__rxq__info.html#a3e84bba8e708a5a37aa1d94dad153832',1,'qbit_rxq_info::qid()'],['../structqbit__txq__info.html#aabe863fee38369fd14d38dcea1d60155',1,'qbit_txq_info::qid()']]],
  ['queue_5fbase_4',['queue_base',['../structport__info.html#aabb1a90a04e0b22fa8fcd1e7c1e61d56',1,'port_info']]],
  ['queue_5fbase_5faddr_5',['queue_base_addr',['../structsystem__register__definition.html#a6d58b2b6f55ad4191194113b6b02bf37',1,'system_register_definition']]],
  ['queue_5fbufsize_6',['queue_bufsize',['../structsystem__register__definition.html#ac54865de7ee8efc9ded2549eeaf06718',1,'system_register_definition']]],
  ['queue_5fmax_5foffset_7',['queue_max_offset',['../structsystem__register__definition.html#abbf28d6f0f44b851dfdf4906a828f33c',1,'system_register_definition']]],
  ['queue_5fpio_5fenable_8',['queue_pio_enable',['../structsystem__register__definition.html#adb5136591dd98a97fc4f43656a1050d5',1,'system_register_definition']]]
];
