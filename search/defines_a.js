var searchData=
[
  ['kernel_5frx_5fcoreid_0',['KERNEL_RX_COREID',['../ekd__util_8h.html#af8c7f3f792552db1a1f36c33c12ddfd1',1,'ekd_util.h']]],
  ['kernel_5frx_5fq_1',['KERNEL_RX_Q',['../ekd__util_8h.html#a2b50f4bc13270fbd7caceef95f2033ab',1,'ekd_util.h']]],
  ['kernel_5ftx_5fcoreid_2',['KERNEL_TX_COREID',['../ekd__util_8h.html#abbb182441716b005bf1af865622b1706',1,'ekd_util.h']]],
  ['kernel_5ftx_5fq_3',['KERNEL_TX_Q',['../ekd__util_8h.html#a3bcee68a443e7dab49ebfe76e55e5c2a',1,'ekd_util.h']]],
  ['keyidrulecheck_5foffset_4',['KEYIDRULECHECK_OFFSET',['../ekd__api_8h.html#adf855fe9bd0d47eecf5825c6541cf062',1,'ekd_api.h']]],
  ['kni_5fenet_5ffcs_5fsize_5',['KNI_ENET_FCS_SIZE',['../ekd__kni_8h.html#a0924eff21ea7ff27eaa1ed60116e31dd',1,'ekd_kni.h']]],
  ['kni_5fenet_5fheader_5fsize_6',['KNI_ENET_HEADER_SIZE',['../ekd__kni_8h.html#abe734a0e86841c838d6d2d12915a4d6c',1,'ekd_kni.h']]],
  ['kni_5finitial_5flcore_7',['KNI_INITIAL_LCORE',['../ekd__kni_8h.html#ae622ab1ec6a5e45d98aa02d17941dece',1,'ekd_kni.h']]],
  ['kni_5fmac_5ftx_5fport_5foffset_8',['KNI_MAC_TX_PORT_OFFSET',['../ekd__kni_8h.html#a30c8a9c9af2c8f8a535c24900bda9547',1,'ekd_kni.h']]],
  ['kni_5fmatching_9',['KNI_MATCHING',['../ekd__kni_8h.html#ae8e858c5f16c6cd0451a9ef3c864403c',1,'ekd_kni.h']]],
  ['kni_5fmax_5fkthread_10',['KNI_MAX_KTHREAD',['../ekd__kni_8h.html#afd06b23abd6c5c4da73573d402819018',1,'ekd_kni.h']]],
  ['kni_5fmax_5fpkt_5fsize_11',['KNI_MAX_PKT_SIZE',['../ekd__kni_8h.html#a58126abcc3b312865da33d7078345aba',1,'ekd_kni.h']]]
];
