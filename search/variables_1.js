var searchData=
[
  ['acknum_0',['acknum',['../structxmit__queue__data.html#a1af64b445fe2639bddf09d85352b2878',1,'xmit_queue_data::acknum()'],['../structxmit__queue__data__mbuf.html#add504ff4686aa68fe73477af414185f3',1,'xmit_queue_data_mbuf::acknum()'],['../structtcp__params.html#a5de45c4d6cfb27a0dc3b9524a3ed6542',1,'tcp_params::acknum()'],['../structxmit__pio__queue__data.html#a9dd03a2f14795210cc551fbaca8da276',1,'xmit_pio_queue_data::acknum()']]],
  ['acknum_5fbk_1',['acknum_bk',['../structtcp__params.html#ac4c93417a129f6c7d3917ef5b282f1d9',1,'tcp_params']]],
  ['active_5fconnections_2',['active_connections',['../ekd__tcp__api_8c.html#abd5d80016ffdabcec1ed9ae704d34d62',1,'ekd_tcp_api.c']]],
  ['add_5ftx_5fdma_5ftemp_3',['add_tx_dma_temp',['../ekd__util_8c.html#a7d25b595819e5ad6100a5f316b6b76df',1,'ekd_util.c']]],
  ['app_5ffull_5ftx_5ftcp_5fpio_5fcntr_4',['app_full_tx_tcp_pio_cntr',['../structheartbeat__statistics__counter.html#a51009dd4ea5f9ffa09e3efd57f7ded6e',1,'heartbeat_statistics_counter']]],
  ['app_5ffull_5ftx_5fudp_5fpio_5fcntr_5',['app_full_tx_udp_pio_cntr',['../structheartbeat__statistics__counter.html#aeb5ec1a210b8ea55d87c78f4c7455eba',1,'heartbeat_statistics_counter']]],
  ['app_5fkey_5fhi_5ftcp_5fpio_5fcntr_6',['app_key_hi_tcp_pio_cntr',['../structheartbeat__statistics__counter.html#adf6ae653fdbf495bf4eddf24fc5c16c9',1,'heartbeat_statistics_counter']]],
  ['app_5fkey_5flo_5ftcp_5fpio_5fcntr_7',['app_key_lo_tcp_pio_cntr',['../structheartbeat__statistics__counter.html#af3c4e277e528545daf27c891b687676c',1,'heartbeat_statistics_counter']]],
  ['app_5fkey_5ftx_5fdma_5fdrop_5fcntr_8',['app_key_tx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#a50b66ad9cfb65fbfb9a0df44ebe6d7b6',1,'heartbeat_statistics_counter']]],
  ['app_5fnotf_5frx_5fdma_5fdrop_5fcntr_9',['app_notf_rx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#ae4f4796408e389242c5a96878616c308',1,'heartbeat_statistics_counter']]],
  ['app_5ftcp_5frx_5fdma_5fdrop_5fcntr_10',['app_tcp_rx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#a0514b921f5c0031b8274cb2ca3ed8403',1,'heartbeat_statistics_counter']]],
  ['app_5ftcp_5ftx_5fdma_5fdrop_5fcntr_11',['app_tcp_tx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#aafbf5876aff1d21f67f56ca433d9131c',1,'heartbeat_statistics_counter']]],
  ['app_5fudp_5frx_5fdma_5fdrop_5fcntr_12',['app_udp_rx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#a0ace2a9d19a336f92af1e3a1dcdf3e5f',1,'heartbeat_statistics_counter']]],
  ['app_5fudp_5ftx_5fdma_5fdrop_5fcntr_13',['app_udp_tx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#a3d97f6519c72e94ea8c0e8f1a1e1a90d',1,'heartbeat_statistics_counter']]],
  ['app_5futcp_5frx_5fdma_5fdrop_5fcntr_14',['app_utcp_rx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#ac662785ff2bb43a504feb377e55d2373',1,'heartbeat_statistics_counter']]],
  ['app_5fuudp_5frx_5fdma_5fdrop_5fcntr_15',['app_uudp_rx_dma_drop_cntr',['../structheartbeat__statistics__counter.html#a17dc4d4045fb1fa38c39556fd05dc556',1,'heartbeat_statistics_counter']]],
  ['arp_5fdha_16',['arp_dha',['../structarp__header.html#afe09bef420cbccb576d5eefd52bdf2d6',1,'arp_header']]],
  ['arp_5fdpa_17',['arp_dpa',['../structarp__header.html#a44627964ed32160e889a11398bb52169',1,'arp_header']]],
  ['arp_5fsha_18',['arp_sha',['../structarp__header.html#adad55abff949925308842fd9f0ceca84',1,'arp_header']]],
  ['arp_5fspa_19',['arp_spa',['../structarp__header.html#aa4e855f57ee8cf78894daa5b7a2413b9',1,'arp_header']]]
];
