var searchData=
[
  ['delay_5fclk_0',['delay_clk',['../examples_2dpi_8h.html#a7c831ad0160ec69c92ba7f27fdf72056',1,'delay_clk(int val):&#160;dpi.h'],['../include_2dpi_8h.html#a7c831ad0160ec69c92ba7f27fdf72056',1,'delay_clk(int val):&#160;dpi.h'],['../src_2dpi_8h.html#a7c831ad0160ec69c92ba7f27fdf72056',1,'delay_clk(int val):&#160;dpi.h']]],
  ['delay_5fns_1',['delay_ns',['../examples_2dpi_8h.html#ab850539229315b69be1f579e1c63983d',1,'delay_ns(int val):&#160;dpi.h'],['../include_2dpi_8h.html#ab850539229315b69be1f579e1c63983d',1,'delay_ns(int val):&#160;dpi.h'],['../src_2dpi_8h.html#ab850539229315b69be1f579e1c63983d',1,'delay_ns(int val):&#160;dpi.h']]],
  ['delay_5fpkt_5fclk_2',['delay_pkt_clk',['../examples_2dpi_8h.html#ade311d340f2a80ea6d16a99a1331299a',1,'delay_pkt_clk(int val):&#160;dpi.h'],['../include_2dpi_8h.html#ade311d340f2a80ea6d16a99a1331299a',1,'delay_pkt_clk(int val):&#160;dpi.h'],['../src_2dpi_8h.html#ade311d340f2a80ea6d16a99a1331299a',1,'delay_pkt_clk(int val):&#160;dpi.h']]],
  ['dev_5fremove_5fcallback_3',['dev_remove_callback',['../ekd__util_8c.html#a96d52fdfd93629ff1a39899428dfcc4a',1,'ekd_util.c']]],
  ['dev_5freset_5fcallback_4',['dev_reset_callback',['../ekd__util_8c.html#aa93414a3d2c3f108540b640cdfce6784',1,'ekd_util.c']]],
  ['disable_5frecv_5',['disable_recv',['../ekd__diagnostic_8c.html#a3a82899c89634146ce190cddbe581453',1,'ekd_diagnostic.c']]],
  ['disable_5frx_5fpio_6',['disable_rx_pio',['../ekd__api_8c.html#ac48e8b9fd823f0887de1653df23ff94a',1,'ekd_api.c']]],
  ['disable_5fxmit_7',['disable_xmit',['../ekd__diagnostic_8c.html#a8d56e215f0100ad88264c84d3b10e06f',1,'ekd_diagnostic.c']]],
  ['dump_5fopen_8',['dump_open',['../ekd__util_8h.html#a0f73c6f6c720c87ddfba634202184e5d',1,'ekd_util.h']]]
];
