var searchData=
[
  ['notif_5frx_5fq_0',['NOTIF_RX_Q',['../ekd__util_8h.html#aa61f645e74d72c1c9ae06ef5e460f3d7',1,'ekd_util.h']]],
  ['notif_5ftx_5fq_1',['NOTIF_TX_Q',['../ekd__util_8h.html#a4f1eb1accc451a3e086a23716f9f0b9b',1,'ekd_util.h']]],
  ['notification_5frx_5fcoreid_2',['NOTIFICATION_RX_COREID',['../ekd__util_8h.html#af6ac167fcf0ff08859bc4a572f0418ce',1,'ekd_util.h']]],
  ['notification_5ftx_5fcoreid_3',['NOTIFICATION_TX_COREID',['../ekd__util_8h.html#a5a7cb213e0e7e9279f4be23152b7be0a',1,'ekd_util.h']]],
  ['num_5fdesc_5fper_5fring_4',['NUM_DESC_PER_RING',['../ekd__api_8h.html#a1a652d6b315482713d1838fa0131a270',1,'NUM_DESC_PER_RING():&#160;ekd_api.h'],['../ekd__util_8h.html#a1a652d6b315482713d1838fa0131a270',1,'NUM_DESC_PER_RING():&#160;ekd_util.h']]],
  ['num_5fmbufs_5',['NUM_MBUFS',['../ekd__util_8h.html#ae543d743ada71b167a5cbd813b15fa7f',1,'ekd_util.h']]],
  ['num_5frx_5fpio_5fqueues_6',['NUM_RX_PIO_QUEUES',['../ekd__api_8h.html#a881293eb53b5c1668dd8675108070b00',1,'ekd_api.h']]],
  ['num_5frx_5fpkts_7',['NUM_RX_PKTS',['../ekd__api_8h.html#ac60dce7a35ed59d35e2b6ce1755e11c0',1,'NUM_RX_PKTS():&#160;ekd_api.h'],['../ekd__util_8h.html#ac60dce7a35ed59d35e2b6ce1755e11c0',1,'NUM_RX_PKTS():&#160;ekd_util.h']]]
];
