var searchData=
[
  ['kernel_5frx_5fthread_5fcosim_0',['kernel_rx_thread_cosim',['../ekd__diagnostic_8c.html#a7dc105de2c00cbf02dd90352ad5889f8',1,'ekd_diagnostic.c']]],
  ['kni_5fadd_5ftx_5fdma_5fheader_5fmbuf_1',['kni_add_tx_dma_header_mbuf',['../ekd__kni_8c.html#a63528c8be6b0c6cd15de4a16cc5617a7',1,'ekd_kni.c']]],
  ['kni_5falloc_2',['kni_alloc',['../ekd__kni_8c.html#aff60d639643fd2d054e9ceb25ab6ada8',1,'ekd_kni.c']]],
  ['kni_5fburst_5ffree_5fmbufs_3',['kni_burst_free_mbufs',['../ekd__kni_8c.html#a1614e4f5891e4063abc191900716770b',1,'ekd_kni.c']]],
  ['kni_5fchange_5fmtu_4',['kni_change_mtu',['../ekd__kni_8c.html#a4cdd33f5d6e34a9c97b837ff108d8475',1,'ekd_kni.c']]],
  ['kni_5fconfig_5fmac_5faddress_5',['kni_config_mac_address',['../ekd__kni_8c.html#a2609d5f9bd6703ee9c204007d006a3dd',1,'ekd_kni.c']]],
  ['kni_5fconfig_5fnetwork_5finterface_6',['kni_config_network_interface',['../ekd__kni_8c.html#af92e96f11452d26e0455e4757445ddf2',1,'ekd_kni.c']]],
  ['kni_5ffree_5fkni_7',['kni_free_kni',['../ekd__kni_8c.html#a5b971cba37c462ad24a82a100de8f3ba',1,'ekd_kni.c']]],
  ['kni_5fprint_5fstats_8',['kni_print_stats',['../ekd__kni_8h.html#acfce06e6eb3287af89e81cfab52cdb07',1,'kni_print_stats(void):&#160;ekd_kni.c'],['../ekd__kni_8c.html#acfce06e6eb3287af89e81cfab52cdb07',1,'kni_print_stats(void):&#160;ekd_kni.c']]],
  ['kni_5ftx_5fpio_9',['kni_tx_pio',['../ekd__kni_8c.html#a3bd0a9d08171ee55d561932dcd737769',1,'ekd_kni.c']]]
];
